const request = require('request');

// 开始时间
const startTime = 1621476000000; // 2021-05-20 10:00:00
const secKillOffset = 10000; // 前后10s
const secKillStartTime = startTime - secKillOffset;
const secKillEndTime = startTime + secKillOffset;

// 设置
const userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36';

// 用户设置
const accessToken = 'O0NOBI8TSjcAFVD33LdH0gAAAAAAAAAB'; // in local storage

// 创建订单数据
const createOrderData = {
    "itemToOrderList": [{
        "activityId": "1229074676379025408",
        "dealerId": "5000000001010551",
        "itemId": 2494,
        "quantity": 1,
        "skuId": 4920
    }],
    "orderChannel": 1,
    "cartIds": [13109],
    "terminal": 1,
    "vmId": 1
};

function createOrder() {
    console.log('创建订单...');

    const url = 'https://mall-app.chery.cn/api/v1/app/trade/buy?access_token=' + accessToken;
    request.post({
        url: url,
        json: true,
        body: createOrderData,
        headers: {
            'accept-language': 'zh-cn',
            'origin': 'https://mall.chery.cn',
            'referer': 'https://mall.chery.cn/',
            'user-agent': userAgent
        },
    }, function (error, response, body) {
        if (error) {
            console.log(error);
            return false;
        }

        if (body && body.status === 200 && body.message === 'maskit.success.general' && body.data) {
            const orderId = body.data;
            console.log('订单号:', orderId);
            confirmOrder(orderId);
        } else {
            console.log('创建订单失败:', body);
        }
    });
}

function confirmOrder(orderId) {
    console.log('确认订单...');

    // 提交数据
    const confirmOrderData = {
        "orderId": orderId,
        "vin": "",
        "userAddressId": "2101103349075168978"
    };

    const url = 'https://mall-app.chery.cn/api/v1/app/trade/confirm?access_token=' + accessToken;
    request.post({
        url: url,
        json: true,
        body: confirmOrderData,
        headers: {
            'accept-language': 'zh-cn',
            'origin': 'https://mall.chery.cn',
            'referer': 'https://mall.chery.cn/',
            'user-agent': userAgent
        },
    }, function (error, response, body) {
        if (error) {
            console.log(error);
            return false;
        }

        if (body && body.status === 200 && body.message === 'maskit.success.general') {
            console.log('Confirm status:', body.data);
        } else {
            console.log('Invalid response:', body);
        }
    });
}

function start() {
    // 当前时间戳
    const currentTimestamp = new Date().getTime();

    // 判断秒杀是否开始
    if (currentTimestamp < secKillStartTime) {
        const leftSeconds = parseInt((secKillStartTime - currentTimestamp) / 1000);
        console.log('秒杀还未开始:' + leftSeconds + '秒后开始');
    } else if (currentTimestamp > secKillEndTime) {
        console.log('秒杀已结束');
    } else {
        // 开始秒杀
        console.log('秒杀中...');
        try {
            createOrder();
        } catch (e) {
            console.log(e);
        }
    }
}


// 输出提示信息
console.log('================= Start Script ======================');
console.log('API Access Token:', accessToken);
console.log('活动开始时间:', new Date(startTime).toLocaleString());
console.log('秒杀开始时间:', new Date(secKillStartTime).toLocaleString());
console.log('秒杀结束时间:', new Date(secKillEndTime).toLocaleString());
console.log('=====================================================');

setInterval(start, 100);
